/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ues.occ.edu.sv.ingenieria.prn335.guia01.control;

import java.util.ArrayList;
import ues.occ.edu.sv.ingenieria.prn335.guia01.entity.Consumible;

/**
 *
 * @author estuardo
 */
public class Cine {

    ArrayList<Consumible> listaConsumibles = new ArrayList<>();
    ArrayList<Consumible> listaConsumiblesNueva = new ArrayList<>();

    public Cine() {

        listaConsumibles.add(new Consumible(1, "Soda", "Pequeño", 2, 2.5));
        listaConsumibles.add(new Consumible(2, "Pop Corn", "Mediano", 3, 3.75));
        listaConsumibles.add(new Consumible(3, "Nachos", "Grande", 5, 6.25));
        listaConsumibles.add(new Consumible(4, "Chocolate M&M", "Mediano", 2, 2.5));
        listaConsumibles.add(new Consumible(5, "Hot dog", "Grande", 5, 6.25));
        listaConsumibles.add(new Consumible(6, "Depresión", "Gigante", 1, 10));
        listaConsumibles.add(new Consumible(7, "Pop Corn Caramelizado", "Mediano", 5, 6.75));
        listaConsumibles.add(new Consumible(8, "Tortilla", "Grande", 7, 8.50));
        listaConsumibles.add(new Consumible(9, "Chocolate M&M", "Mediano", 2, 2.5));
        listaConsumibles.add(new Consumible(10, "Kellogs", "Grande", 7, 9.25));

    }

    /**
     * Agrega un consumible a la lista de consumibles
     *
     * @param id_consumible
     * @param nombre
     * @param tamanio
     * @param precioBase
     * @param precioVenta
     */
    public void agregarConsumible(int id_consumible, String nombre, String tamanio, double precioBase, double precioVenta) {

        listaConsumibles.add(new Consumible(id_consumible, nombre, tamanio, precioBase, precioVenta));

    }

    public ArrayList<Consumible> filtrarConsumible() {
        for (int i = 0; i < listaConsumibles.size(); i++) {
            if (listaConsumibles.get(i).getTamanio().equals("Mediano") && listaConsumibles.get(i).getPrecioVenta() >= 4.50 && listaConsumibles.get(i).getPrecioVenta() <= 6.85) {
                listaConsumiblesNueva.add(listaConsumibles.get(i));
            } else if (listaConsumibles.get(i).getTamanio().equals("Grande")) {
                listaConsumiblesNueva.add(listaConsumibles.get(i));
            }
        }
        return listaConsumiblesNueva;
    }

    public void eliminarConsumible(int id_consumible) {
        if (listaConsumibles.get(id_consumible - 1).getPrecioVenta() >= 8 && listaConsumibles.get(id_consumible - 1).getPrecioVenta() <= 10) {

            if (listaConsumibles.get(id_consumible - 1).getPrecioBase() > 5) {

            }
        } else {

            listaConsumibles.remove((id_consumible - 1));

        }
    }

    //Devuelve la listaCosumibles
    public ArrayList<Consumible> getListaConsumibles() {
        return listaConsumibles;
    }

}
