/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ues.occ.edu.sv.ingenieria.prn335.guia01.entity;

/**
 *
 * @author estuardo
 */
public class Consumible {
     private int id_consumible;
    private String nombre;
    private String tamanio;
    private double precioBase;
    private double precioVenta;


    public Consumible(int id_consumible, String nombre, String tamanio, double precioBase, double precioVenta){
        this.id_consumible = id_consumible;
        this.nombre = nombre;
        this.tamanio = tamanio;
        this.precioBase = precioBase;
        this.precioVenta = precioVenta;
    }
    public int getId_consumible() {
        return id_consumible;
    }

    public void setId_consumible(int id_consumible) {
        this.id_consumible = id_consumible;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getTamanio() {
        return tamanio;
    }

    public void setTamanio(String tamanio) {
        this.tamanio = tamanio;
    }

    public double getPrecioBase() {
        return precioBase;
    }

    public void setPrecioBase(double precioBase) {
        this.precioBase = precioBase;
    }

    public double getPrecioVenta() {
        return precioVenta;
    }

    public void setPrecioVenta(double precioVenta) {
        this.precioVenta = precioVenta;
    }
        
}
